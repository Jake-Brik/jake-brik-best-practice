from bs4 import BeautifulSoup
import requests

URL = "https://jetlend.ru/"


def all_tags_and_tags_with_attrs_counter(url: str) -> list[int, int]:
    html_content = requests.get(url).text
    soup = BeautifulSoup(html_content, "html.parser")

    all_tags_counter = 0
    tags_with_attrs_counter = 0
    for tag in soup.find_all():
        all_tags_counter += 1
        if tag.attrs:
            tags_with_attrs_counter += 1
    return [all_tags_counter, tags_with_attrs_counter]


all_tags, tags_with_attrs = all_tags_and_tags_with_attrs_counter(URL)
print(f"Total HTML tags on page {URL}: {all_tags}")
print(f"HTML tags with attributes {URL}: {tags_with_attrs}")

#  OUTPUT:
#  Total HTML tags on page https://jetlend.ru/: 969
#  HTML tags with attributes https://jetlend.ru/: 889