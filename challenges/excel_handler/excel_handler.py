import pandas as pd
from pathlib import Path

EXCEL_FILE_PATH = "database.xlsx"
NEEDED_DATE = "2021-12-31"


def filter_by_date(excel_file_path: str, date: str) -> None:
    excel_file_path = Path(excel_file_path)
    needed_date = pd.to_datetime(date)
    df = pd.read_excel(excel_file_path, sheet_name=0, header=0)
    filtered_df = df[(df['registration_date'] <= needed_date)]
    amount_sum = int(filtered_df.sum(numeric_only=True)[-1:])
    print(f"Amount of all companies registered not later than 2021: {amount_sum}")


def group_by_rating_and_amount_sum(excel_path: str) -> None:
    excel_path = Path(excel_path)
    df = pd.read_excel(excel_path, sheet_name=0)
    rating_amount = df.groupby('rating')['amount'].sum()
    rating_amount.to_excel('rating_amount.xlsx', sheet_name='rating_amount')


filter_by_date(EXCEL_FILE_PATH, NEEDED_DATE)
group_by_rating_and_amount_sum(EXCEL_FILE_PATH)