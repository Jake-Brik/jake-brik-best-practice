from dataclasses import dataclass


@dataclass
class ScalesIterationCounter:
    """This is a Scales Iteration Counter.
    It can count needed iterations for different amount of needed weight and reference weight.
    Needed weight can be any positive integer.
    Reference weight can be from 1 to 31 for correct algorythm working.
    """

    _needed_weight: int = 1000
    _reference_weight: int = 1

    @property
    def needed_weight(self):
        return self._needed_weight

    @needed_weight.setter
    def needed_weight(self, value: int):
        if value < self._reference_weight:
            raise ValueError(
                f"For correct working needed weight must be bigger than reference. "
                f"Current reference weight {self._reference_weight}"
            )
        if type(value) != int:
            raise ValueError("Needed weight can be only integer")
        self._needed_weight = value

    @property
    def reference_weight(self):
        return self._reference_weight

    @reference_weight.setter
    def reference_weight(self, value: int):
        if value < 1 or value >= 31:
            raise ValueError("For correct working reference weight can be from 1 to 31")
        if type(value) != int:
            raise ValueError("Reference weight can be only integer")
        self._reference_weight = value

    def count(self):
        iterations_counter = 1
        result = self._reference_weight
        needed_weight = self._needed_weight
        while result != needed_weight:
            if result * 2 > needed_weight:
                needed_weight = needed_weight - result
                result = self._reference_weight
                if needed_weight < 0:
                    print(
                        f"Incorrect input data: reference weight '{self._reference_weight}' "
                        f"bigger than remainder of division '{-needed_weight}'"
                    )
                    break
                continue
            if result == 32:
                result = (result - self._reference_weight) * 2
            elif result == 62:
                result = (result * 2) + self._reference_weight
            else:
                result *= 2
            iterations_counter += 1
        return iterations_counter
