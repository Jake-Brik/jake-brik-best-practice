from csv import writer, QUOTE_MINIMAL
from multiprocessing import Manager, Pool, cpu_count, Queue
from random import randint
from secrets import choice
from string import ascii_letters, digits
from uuid import uuid4, UUID
from xml.etree.ElementTree import parse
from pathlib import Path
from zipfile import ZipFile

from templates.xml_template import XML_TEMPLATE
from templates.objects_template import OBJECTS_TEMPLATE
from database import UNIQUE_ID_DATABASE
from settings import (
    OUTPUT_FOLDER_PATH,
    XML_AMOUNT,
    ZIP_AMOUNT,
    LEVEL_VALUE_MIN,
    LEVEL_VALUE_MAX,
    OBJECT_NAME_SYMBOLS_MAX,
    OBJECTS_MAX,
)


def get_unique_value() -> str:
    unique_value = uuid4()
    while not additional_unique_check(unique_value):
        unique_value = uuid4()
    return str(unique_value)


def additional_unique_check(value: UUID) -> bool:
    if value in UNIQUE_ID_DATABASE:
        return False
    UNIQUE_ID_DATABASE.add(value)
    return True


def get_random_number(
    min_number: int = LEVEL_VALUE_MIN, max_number: int = LEVEL_VALUE_MAX
) -> int:
    return randint(min_number, max_number)


def get_objects_data(objects_number: int) -> str:
    objects_data = ""
    for i in range(objects_number):
        symbols = randint(1, OBJECT_NAME_SYMBOLS_MAX)
        random_string = "".join(choice(ascii_letters + digits) for _ in range(symbols))
        objects_data += OBJECTS_TEMPLATE.format(random_string=random_string) + "\n"
    return objects_data


def create_xml() -> str:
    random_unique_string = get_unique_value()
    random_number = get_random_number()
    objects_number = randint(1, OBJECTS_MAX)
    objects_data = get_objects_data(objects_number)
    correct_template = XML_TEMPLATE.format(
        random_unique_string=random_unique_string,
        random_number=random_number,
        objects_data=objects_data,
    )
    return correct_template


def make_zip_with_xml(out_path: Path, zip_quantity: int, xml_quantity: int) -> None:
    out_path.mkdir(parents=True, exist_ok=True)
    for zip_number in range(zip_quantity):
        zip_name = str(zip_number) + ".zip"
        with ZipFile(out_path / zip_name, "w") as zip_file:
            for xml_number in range(xml_quantity):
                xml_body = create_xml()
                xml_name = str(xml_number) + ".xml"
                zip_file.writestr(xml_name, xml_body)


def worker(file: Path, id_level_queue: Queue, id_object_name_queue: Queue) -> None:
    with ZipFile(file) as zip_file:
        for xml_file in zip_file.namelist():
            with zip_file.open(xml_file, "r") as xml_file_body:
                tree = parse(xml_file_body)
                root = tree.getroot()
                id_value = root[0].attrib["value"]
                level_value = root[1].attrib["value"]
                objects_value = [object_name.attrib["name"] for object_name in root[2]]
                id_level_queue.put([id_value, level_value])
                id_object_name_queue.put([id_value, objects_value])


def listener(out_path: Path, name: str, queue: Queue):
    with open(out_path / (name + ".csv"), "w", newline="") as csv_file:
        csv_writer = writer(csv_file, delimiter=",", quoting=QUOTE_MINIMAL)
        while True:
            xml_data = queue.get()
            if xml_data == "Finished":
                break
            if name == "id_level":
                csv_writer.writerow([xml_data[0], xml_data[1]])
            elif name == "id_object_name":
                id_value = xml_data[0]
                for i in xml_data[1]:
                    csv_writer.writerow([id_value, i])
                    id_value = ""


def create_csv(out_path: Path) -> None:
    manager = Manager()
    id_level_queue = manager.Queue()
    id_object_name_queue = manager.Queue()
    pool = Pool(cpu_count())
    id_level_watcher = pool.apply_async(
        listener, (out_path, "id_level", id_level_queue)
    )
    id_object_name_watcher = pool.apply_async(
        listener, (out_path, "id_object_name", id_object_name_queue)
    )
    jobs = []
    for file in sorted(out_path.iterdir()):
        if file.suffix != ".zip":
            continue
        job = pool.apply_async(worker, (file, id_level_queue, id_object_name_queue))
        jobs.append(job)

    for job in jobs:
        job.get()

    id_level_queue.put("Finished")
    id_object_name_queue.put("Finished")
    pool.close()
    pool.join()


def main():
    out_path = Path(OUTPUT_FOLDER_PATH)
    make_zip_with_xml(out_path, ZIP_AMOUNT, XML_AMOUNT)
    create_csv(out_path)


if __name__ == "__main__":
    main()
