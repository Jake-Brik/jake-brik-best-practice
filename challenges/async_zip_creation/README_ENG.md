## Write a Python program that does the following:
~~~
1. Creates 50 zip archives, each containing 100 xml files with random data of the following structure:
<root>
<var name='id' value='<random unique string value>'/>
<var name='level' value='<random number between 1 and 100>'/>
<objects>
<object name='<random string value>'/>
<object name='random string value>'/>
...
</objects>
</root>
The objects tag contains a random number (1 to 10) of nested object tags.
2. it processes the directory with the zip-archives, parses the attached xml files and generates two csv files:
First: id, level - one line per xml file
Second: id, object_name - a separate line for each tag object (will have from 1 to 10 lines per xml file)
It is very desirable to make job 2 use efficiently the resources of multicore processor.
~~~