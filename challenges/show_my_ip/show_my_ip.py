from requests import get

IP_SERVICE_LINK = "https://www.ifconfig.me"


def get_ip(link: str) -> str:
    ip = get(link).content.decode("utf8")
    return ip


my_ip = get_ip(IP_SERVICE_LINK)
print(f"My public IP address is: {my_ip}")