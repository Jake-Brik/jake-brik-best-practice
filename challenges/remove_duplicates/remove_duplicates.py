def remove_duplicates(data: list[dict]) -> list[dict]:
    result = []
    verification_set = set()
    for dictionary in data:
        tuple_for_verification = tuple(dictionary.items())
        if tuple_for_verification not in verification_set:
            verification_set.add(tuple_for_verification)
            result.append(dictionary)
    return result