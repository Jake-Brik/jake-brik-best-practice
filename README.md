# Jake Brik Best Practice

## Project Objective

I made this all-in-one project to showcase my highly skilled programming abilities and best practices in Python.<br/>
I will use the most popular current technologies and will add to it as I acquire new competencies.<br/>
I will also show my skills in solving various unrelated problems to show additional skills that are not involved in the main project.<br/>

### My contacts:<br/>
Telegram:<br/>
@jake_spiritus<br/>
Email:<br/>
spiritus.jake@gmail.com<br/>